image: registry.gitlab.com/kimlab/linux-anvil:latest

stages:
  - build
  - test
  - deploy

cache:
  key: ${CI_COMMIT_REF_NAME}
  paths:
    - .conda_packages

# === Configuration ===

variables:
  PYTHON_VERSION: "3.6"

.configure: &configure
  before_script:
    # Conda environment variables
    - export CONDA_PKGS_DIRS="${CI_PROJECT_DIR}/.conda_packages"
    # Make conda cache and bld folders
    - mkdir -p "${CI_PROJECT_DIR}/conda-bld" "${CONDA_PKGS_DIRS}"
    - conda config --add pkgs_dirs "${CONDA_PKGS_DIRS}"
    # Channels
    - conda config --add channels ${CI_PROJECT_NAMESPACE}
    - conda config --add channels defaults
    - conda config --add channels conda-forge
    - conda config --add channels pytorch
    - conda update -y -q --no-channel-priority conda conda-build

# === Build ===

build:
  stage: build
  <<: [*configure]
  script:
    # Install modern gcc stack (needed for both building and testing)
    - sudo curl -o/etc/yum.repos.d/devtools-2.repo -L --silent
      http://people.centos.org/tru/devtools-2/devtools-2.repo
    - sudo yum update -y -q
    - sudo yum install -y -q devtoolset-2-toolchain perl
    # Build conda packages
    - cd $CI_PROJECT_DIR/recipe
    - conda build .
      --python $PYTHON_VERSION
      --output-folder "${CI_PROJECT_DIR}/conda-bld" 
      --no-test
      2>&1 | tee "${CI_PROJECT_DIR}/build.log"
  artifacts:
    when: always
    paths:
      - conda-bld
      - build.log
    
# === Test ===

test:
  stage: test
  <<: [*configure]
  script:
    # Restore built packages
    - cp -r $CI_PROJECT_DIR/conda-bld/* /opt/conda/conda-bld/
    - conda index /opt/conda/conda-bld/
    # Run tests
    - cd $CI_PROJECT_DIR/recipe
    - conda build .
      --test
      --python $PYTHON_VERSION
  artifacts:
    paths:
      - environment-py${PYTHON_VERSION/./}.yml

# === Deploy ===

deploy:
  stage: deploy
  script:
    - if [[ ${CI_COMMIT_REF_NAME} = 'master' ]] ; then
        anaconda -t $ANACONDA_TOKEN upload $CI_PROJECT_DIR/conda-bld/linux-64/*.tar.bz2 -u ${CI_PROJECT_NAMESPACE} --force --no-progress ;
      else
        anaconda -t $ANACONDA_TOKEN upload $CI_PROJECT_DIR/conda-bld/linux-64/*.tar.bz2 -u ${CI_PROJECT_NAMESPACE} --label dev --force --no-progress ;
      fi
  dependencies:
    - build
  except:
    - triggers
