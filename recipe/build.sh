#!/bin/bash

export AS="/opt/rh/devtoolset-2/root/usr/bin/as"
export AR="/opt/rh/devtoolset-2/root/usr/bin/ar"
export CC="/opt/rh/devtoolset-2/root/usr/bin/gcc"
export CXX="/opt/rh/devtoolset-2/root/usr/bin/g++"
export PATH="/opt/rh/devtoolset-2/root/usr/bin:${PATH}"

mkdir build
cd build
cmake .. -DCMAKE_INSTALL_PREFIX=$PREFIX -DCMAKE_POSITION_INDEPENDENT_CODE:BOOL=true
make -j$(getconf _NPROCESSORS_CONF)
make install
cd ..